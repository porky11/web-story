This is a server for publishing interactive stories.
It depends on my other lib [simple-petri-net](https://gitlab.com/porky11/simple-petri-net).

There is just a single function you need to create the server:

*Function*
`run &key port path`

`port`: Your local port, in which you can access the story using your browser

`path`: A path in the file system, where your interactive story is saved

It's assumed, there already exist some files in path:

`{path}.{lang}.story`

This file contains the story. You may create multiple files for the story in different langauges.
The languages are defined in the file `{lang}.lang`.
The story is splitted in many scenes. The scenes are seperated by multiple newlines. The first line of a story is the identifier of the story. All other lines are the text of the scene.
Sometimes you may decide between multiple scenes. In that case, you will see the first text line of the scene, which is the second line.

A story file may look like this:
```
intro
The story starts here.
Now you can go left or right.
What do you want to do?

left
Go left.
You went left.

right
Go right.
You went right.
```

`{path}.pn`

This file defines the connections between the stories. If you want to know, how it has to look, see [simple-petri-net](https://gitlab.com/porky11/simple-petri-net).
A useful file for the story in the story-file looks like that:
```
left|right: intro: start*
```

`{path}.keys`

In this file, some keys are defined. For every text line in the story file, the keys get replaced by user defined values, when you mark them as keys using `*`.
When a new user begins, he has to select a name and values for every key in the key file.
The keys may have default values. Keys are just alphanumeric words which may contain `-`, seperated by whitespaces.
In order to give a key a default value, add a `=` after it and then let the default value follow, which also has to be alphanumeric and may contain `-`. This way it's only possible to replace single words with single words using keys.
Some example key file may look like this:
```
age home=China
```
Some text line of a story may then look like this:
```
Your name is *name, you are *age years old and live in *home.
```
When you set your name to `John` and your age to `21` and leave default value for home, this line will be expanded to following text when playing:
```
Your name is John, you are 21 years old and live in China.
```

`{path}.lang`

This file contains the languages, your story is avalable in.
This has the same format as the keyfile.
At start, you may select between multiple languages using checkboxes.
The first element of this file is the default langauge.
If a language has a default value, this value will be written next to the checkbox, else the key will be written.
The key will be used to identify the language.
An example for a language file would be following:
```
en=English
fr=French
de=German
```
When this is your language file, you have to define three story files, called `{path}.en.story`, `{path}.fr.story`, `{path}.de.story`.
English will be selected by default.
Additionally, if you don't specify a language, `en` will be assumed as language identifier.
You see, the structure of the story is defined seperated by the text. That's an important reason for splitting these files. Some engines don't split text from the structure, like one of the most popular engines for visual novels, so it's difficult to translate it in an elegant way.

`{path}.{name}.state`

These files won't be created by you. If someone plays a story, this file will save the current state of the story, when you shoutdown the server. `name` is the name, the player chooses.
So when you restart the server, even if you restart lisp, the player will be able to continue, where he stopped playing.
The files will only be created, if you shut down the server.
To shut down the server, just input a newline into `*standard-input*`, where the server is running.

So your working directory, may look like this in the end, when you specify your path to be `path/to/test`.
```
test.lang
test.pn
test.keys
test.en.story
test.fr.story
test.en.story
test.testuser.state
test..state
test.John.state
```

That's it. Now have fun creating your interactive stories and letting other people play them over your web server.

