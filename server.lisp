(defpackage+-1:defpackage+ #:web-story
  (:use #:cl :cl-who #:split-sequence #:hunchentoot #:petri-net #:petri-parser #:petri-test)
  (:export #:run
           #:reload))
(in-package #:web-story)

(defvar *languages* (list "en"))

(defvar *table* nil)

(defvar *story* nil)

(defvar *path* "")

(defvar *state* nil)

;;(defvar *val*)
;;(defvar *debug*)

(defun get-story (lang)
  (let ((lang (or lang "en")))
    (or (gethash lang *story*)
        (setf (gethash lang *story*) (load-story (format nil "~a~@[.~a~].story" *path* lang))))))

(defun reload (path)
  (setf *keys* (get-list (format nil "~a.keys" path)))
  (setf *languages* (get-list (format nil "~a.lang" path)))
  (setf *path* path))

(defun save-state (name list)
  (with-open-file (stream (format nil "~a.~a.state" *path* name) :direction :output :if-exists :supersede)
    (dolist (s (reverse list))
      (write-line s stream))))

(defun get-state (name)
  (get-file (format nil "~a.~a.state" *path* name)))
           

(defun save-states ()
  (maphash #'save-state *state*))

(defun run (&key (port 1247)  (path "~/Programming/vn/city/city"))
  (let ((instance (make-instance 'easy-acceptor :port port)))
    (reload path)
    (setf *table* (make-hash-table :test #'equal))
    (setf *story* (make-hash-table :test #'equal))
    (setf *state* (make-hash-table :test #'equal))
    (setf *dispatch-table* '(get-function))
    (start instance)
    (read-line)
    (save-states)
    (stop instance)))

(defun replace-line (line)
  (loop for char in (coerce line 'list)
     with name
     with key
     with result
     do (progn
          (if (char= char #\*)
              (setf name t)
              (if name
                  (if (or (alphanumericp char) (member char '(#\-) :test #'char=))
                      (push char key)
                      (let ((value (get-key (coerce (reverse key) 'string))))
                        (dolist (char (coerce value 'list))
                          (push char result))
                        (setf name nil key nil)
                        (push char result)))
                  (push char result))))
     finally (return (coerce (reverse result) 'string))))

(defun write-story (story stream)
  (with-html-output (stream)
    (dolist (line story)
      (htm (str (replace-line line)) :br))
    :br))

(defun petri-story (value name stream story)
  (with-html-output (stream)
    (loop while (and value (transition-callable-p value)) do
         (progn
           (call-transition value)
           (push value (gethash name *state*))
           (let ((story (gethash value story)))
             (dolist (line story)
               (htm (str (replace-line line)) :br)))
           (htm :br)
           (let ((next (list-active-transitions)))
             (let ((val (car next)))
               (setf value (unless (cdr next) val))))))))

(defun petri-step (stream story)
  (with-html-output (stream)
    (let ((transitions (list-active-transitions)))
      (dolist (ts transitions)
        (let ((title (car (gethash ts story))))
          (when title
            (htm
             (:a :href (format nil "/play/~a" ts)
                 (:b (str (replace-line title))))
             :br :br)))))))
#+(or)
(defun petri-back-step (stream keys)
  (with-html-output (stream)
    (let ((transitions (list-current-transitions)))
      (dolist (ts transitions)
        (let ((title (car (gethash ts *story*))))
          (htm
           (:a :href (format nil "/back/~a?~{~{~a=~a~}~^&~}" ts keys)
               (:b (str title)))
           :br))))))

(defvar *keys* nil)

(defun get-key (key)
  (gethash key (session-value 'keys)))

(defun get-function (request)
  (let ((uri (request-uri request)))
    (lambda (&optional back)
      (setf (hunchentoot:content-type*) "text/html")
      (start-session)
      (unless (session-value 'keys)
        (setf (session-value 'keys) (make-hash-table :test #'equal)))
      (destructuring-bind (&optional path keys) (split-sequence #\? uri)
        (declare (ignore keys))
        (dolist (parameter (post-parameters*))
          (setf (gethash (car parameter) (session-value 'keys)) (cdr parameter)))
        (let ((name (get-key "name"))
              (lang (get-key "_lang")))
          (destructuring-bind (&optional type value &rest rest)
              (remove "" (split-sequence #\/ path) :test #'string=)
            (declare (ignore rest))
            (setf value (coerce (loop for char in (coerce value 'list)
                                   with skip = 0
                                   if (eq char #\%)
                                   do (setf skip 3)
                                   if (= skip 0)
                                   collect char
                                   else if (= skip 1)
                                   collect (progn (decf skip) #\Space)
                                   else
                                   do (decf skip))
                                'string))
            (if (string= type "reset")
                (setf (gethash name *table*) (get-petri-net)
                      (gethash name *state*) nil)
                (if (string= type "back")
                    (setf back t)))
            (unless type
              (unless (parameter "name")
                (setf name nil)))
            (with-output-to-string (stream)
              (let ((story (get-story lang)))
                (if name
                    (progn
                      (let ((petri
                             (or (gethash name *table*)
                                 (setf (gethash name *table*) (get-petri-net)))))
                        (with-petri-net petri
                          ;;#+(or)
                          ;;(setf *val* 0 *debug* type)
                          (when (= (length type) 0)
                            (when (= (length value) 0)
                              (unless (gethash name *state*)
                                (let ((state (get-state name)))
                                  (setf (gethash name *state*) (reverse state))
                                  (with-html-output (stream)
                                    (dolist (val state)
                                      (call-transition val)
                                      (let ((story (gethash val story)))
                                        (dolist (line story)
                                          (htm (str (replace-line line)) :br)))
                                      (htm :br)))))))
                          (if back
                              (let ((back-name (pop (gethash name *state*))))
                                (uncall-transition back-name)
                                (let ((back-name (car (gethash name *state*))))
                                  (write-story (gethash back-name story) stream)))
                              (petri-story value name stream story))
                          (petri-step stream story)))
                      (with-html-output (stream)
                        :br
                        (:a :href "/back"
                            (:b (str "Rewind")))
                        :br :br
                        (:a :href "/reset"
                            (:b (str "Reset")))))
                    (with-html-output (stream)
                      (htm (:form :method :post
                                  (str "Choose some nice names and other information for the persons in the story") :br
                                  (str "Your name will be used for saving your progress") :br
                                  :br
                                  (str "Enter your name") :br
                                  (:input :type "text" :name "name" :value (get-key "name")) :br
                                  (dolist (key *keys*)
                                    (destructuring-bind (key &optional (value "")) (split-sequence #\= key)
                                      (htm
                                       (str (format nil "Enter name ~S" key)) :br
                                       (:input :type "text" :name key :value (or (get-key key) value)) :br)))
                                  (let ((checked "checked"))
                                    (dolist (key *languages*)
                                      (destructuring-bind (key &optional (value key)) (split-sequence #\= key)
                                        (htm
                                         (:input :type "radio" :name "_lang" :value key :checked checked) (str value)))
                                      (setf checked nil)))
                                  :br
                                  (:input :type "submit"))))))
              (with-html-output (stream)
                :br :br
                (:a :href "/"
                    (:b (str "Back")))))))))))



(defun get-file (filename)
  (with-open-file (stream filename :if-does-not-exist :create)
    (loop for line = (read-line stream nil)
       while line
       collect line)))

(defvar *whitespaces*
  '(#\Space #\Newline #\Backspace #\Tab 
    #\Linefeed #\Page #\Return #\Rubout))

(defun whitespacep (char)
  (member char *whitespaces* :test #'char=))

(defun get-list (file)
  (remove "" (split-sequence-if #'whitespacep (format nil "~{~a ~}" (get-file file))) :test #'string=))

(defun load-story (filename)
  (let ((story (make-hash-table :test #'equal))
        (list (split-sequence ""
                              (mapcar (lambda (line)
                                        (string-trim *whitespaces* line))
                                      (get-file filename))
                              :test #'equal)))
    (loop for (name . value) in list
       do (setf (gethash name story) value))
    story))

(defun get-petri-net ()
  (with-new-petri-net
    (load-file (format nil "~a.pn" *path*))))
